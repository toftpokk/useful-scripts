# Mmeta
Script to view and get audio file metadata

# Usage
`mmeta get` to view metadata
`mmeta set FILENAME TITLE ARTIST` to set filename title and artist
