# Listshasum
creates a shasum of all files in the current directory

## Usage
Edit the `grepkey` variable to change the pattern that listshasum matches
Edit the `sha_algo` to change which algorithm it uses
