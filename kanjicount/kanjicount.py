#!/usr/bin/env python
import sys
if len(sys.argv) > 1:
    infile = sys.argv[1]
else:
    infile="wikipedia_annelid.txt"


char_counts={}
allchars=""

eng_letters = [ x for x in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ]
numbers = [ x for x in "0123456789" ]
hiragnana=[x for x in"ぁぃぅぇぉっあいうえおかがきぎくぐけげこごさざしじすずせぜそぞただちぢつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをんゔゞ゛゜ーｰ"]
katakana=[x for x in"ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモヤャュョヮヵヶユヨラリルレロワヰヱヲンヴヷヸヹヺ・゛゜ーヽヾㇰㇱㇲㇳㇴㇵㇶㇷㇸㇹㇺㇻㇼㇽㇾㇿ"]
others=['\t','\u3000','.',',','…','!','?','♫',' ','「','」','？','！','(',')','、','×','♪','Ｓ','（','）','-','。','[',']']

 
def add_char(c):
    global allchars
    if c in eng_letters or c in hiragnana or c in katakana or c in others or c in numbers:
        return
    if c in char_counts:
        char_counts[c]+=1
    else:
        char_counts[c]=1
        allchars+=c

def read_file():
    with open(infile,'r') as fh:
        file_lines = fh.read().splitlines()
        for i in range(0,len(file_lines)):
            for j in file_lines[i]:
                    add_char(j)

read_file()
print(allchars)
# print(char_counts)
for i,j in char_counts.items():
    print(i,end=" ")
    for k in range(0,j):
        print("#",end="")
    print("\n",end="")
    # print(i,j)
print(infile,len(allchars))
