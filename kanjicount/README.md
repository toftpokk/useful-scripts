# Kanjicount
Counts all kanji of a text file and shows as count or graph

Can used for kanji in songs

# Usage
`kanjicount.py <filename>` or edit `infile` variable

Exceptions should be put into `others` variable
