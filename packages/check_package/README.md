# Check_package
Reads package list ($1) and checks column 2 against pacman.
Useful for testing list of pacman packages in a tab-separated-value file

Prints "No package_name" if package not found

## Usage
`./check_package.sh package_list`

## package list example
```
Cowsay	cowsay	Native	Fun
Fortune-mod	fortune-mod	Native	Fun
Dwarftherapist	dwarftherapist	Native	Fun
LF	lf-git	AUR	File manager
```

## Output Example
```
No lf-git
```
