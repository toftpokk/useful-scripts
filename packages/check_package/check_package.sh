#!/bin/sh
[ -z "$1" ] && exit 1
for PKG in $(awk '{print $2}' $1);
do
	pacman -Si $PKG 1>/dev/null 2>/dev/null || echo "No $PKG"
done
