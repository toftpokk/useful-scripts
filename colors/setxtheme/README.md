# Setxtheme
Sets xresources theme from image

## Usage
Have an image at ~/.config/wall.png and run ./setxtheme

## Dependencies
python
imagemagick
