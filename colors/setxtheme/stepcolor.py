#!/bin/env python3

# Originally by Alan Zucconi https://www.alanzucconi.com/2015/09/30/colour-sorting/

import random
import math
import colorsys
import sys

def generate_random_colours():
    colours_length = 1000
    colours = []
    for i in range(1, colours_length):
            colours.append ( [ random.random(), random.random(), random.random() ])

def step (r,g,b, repetitions=1):
    lum = math.sqrt( .241 * r + .691 * g + .068 * b )
    h, s, v = colorsys.rgb_to_hsv(r,g,b)
    h2 = int(h * repetitions)
    lum2 = int(lum * repetitions)
    v2 = int(v * repetitions)
    return (h2, lum, v2)

def get_color_columns(colours):
    colourcols=[[] for i in range(8)]
    for i in colours:
        hue = step(i[0],i[1],i[2],8)
        colourcols[hue[0]].append(i)
    return colourcols

def print_colours(colours):
    column=int(get_column())
    for i in colours[column]:
        # print(i[0])
        # print(step(i[0],i[1],i[2],8))
        print(f"{int(i[0]*255):03d}{int(i[1]*255):03d}{int(i[2]*255):03d}")

def get_random(colours):
    splitlen=len(colours)//8
    midlen=splitlen//2
    # print(colours[splitlen])
    for i in [0,2,1,5,7,3]:
        col = colours[splitlen*i+midlen]
        rgb_hex = rgb_float_to_hex(col)
        print(rgb_hex)

def print_colour_columns(colourcols):
    for i in colourcols:
        print(i)

def get_main_colors(colours):
    colornames=["red","green","yellow","blue","purple","cyan"]
    color_columns=get_color_columns(colours)
    out=""
    for j in [0,2,1,5,7,3]:
        lencol = len(color_columns[j])
        if lencol == 0:
            sys.stderr.write(f"Error: Empty Color Column {j}({colornames[j]})\n")
            get_random(colours)
            return
        else:
            rgb_hex = rgb_float_to_hex(color_columns[j][lencol//2])
            # print(rgb_hex)
            out+=rgb_hex+"\n"
    print(out,end="")
        # print(f"{int(i[0]*255):03d}{int(i[1]*255):03d}{int(i[2]*255):03d}")

def float_to_hex(f):
    h=hex(int(f*255))[2:]
    if len(h) == 1:
        return "0"+h
    return h

def rgb_float_to_hex(r):
    hx=""
    hx+=float_to_hex(r[0])
    hx+=float_to_hex(r[1])
    hx+=float_to_hex(r[2])
    return hx

def look_ahead(cur,args):
    if cur+1>=len(args):
        return None
    return args[cur+1]

def is_num(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def handleargs(args):
    cur = 1
    while cur<len(args):
        if args[cur] == "-h":
            help()
        elif args[cur] == "-a":
            colours=sort_input()
            out=""
            for i in colours:
                rgb_hex = rgb_float_to_hex(i)
                out+=rgb_hex+"\n"
            print(out,end="")
        elif args[cur] == "-c":
            ahead=look_ahead(cur,args)
            if ahead == None:
                sys.stderr.write(f"Error: Option requires argument")
                break
            if not is_num(ahead):
                sys.stderr.write(f"Error: Argument needs to be a number")
                break

            colnum = int(ahead)
            colours=sort_input()
            out=""
            color_columns=get_color_columns(colours)
            for i in color_columns[colnum]:
                rgb_hex = rgb_float_to_hex(i)
                out+=rgb_hex+"\n"
            print(out,end="")
        elif args[cur] == "-v":
            colours=sort_input()
            colornames=["red","green","yellow","blue","purple","cyan"]
            color_columns=get_color_columns(colours)
            out=""
            for j in [0,2,1,5,7,3]:
                lencol = len(color_columns[j])
                if lencol == 0:
                    sys.stderr.write(f"Error: Empty Color Column {j}({colornames[j]})\n")
                    get_random(colours)
                    return
                else:
                    rgb_hex = rgb_float_to_hex(color_columns[j][lencol-1])
                    # print(rgb_hex)
                    out+=rgb_hex+"\n"
            print(out,end="")
        cur+=1


# Inputs
def get_args():
    return sys.argv

def get_stdin():
    colours=[]
    stdin_file = sys.stdin 
    for line in stdin_file:
        intrgb=tuple(int(line[i:i+2], 16) for i in (0, 2, 4))
        intrgb=[i/255 for i in intrgb]
        colours.append(intrgb)
    return colours

def sort_input():
    colours = get_stdin()
    colours.sort(key=lambda x: step(x[0],x[1],x[2],8))
    return colours

# Helper Functions

def help():
    print( \
"""Usage: stepcolor [OPTION]
Sorts RGB colors using the step method

  -a        prints all sorted colors
  -c NUM        prints all sorted colors in column NUM with num in between 0-7
  -h        prints this help message
  -v        prints the most vibrant colors in the columns
""")

# Main

def main():
    args=get_args()
    if len(args) > 1:
        handleargs(args)
    else:
        colours=sort_input()
        get_main_colors(colours)

main()
