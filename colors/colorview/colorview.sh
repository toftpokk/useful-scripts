#!/usr/bin/sh
usage(){
	echo "Usage: colorview.sh path/to/image"
	exit 1
}
[ -z ${1} ] && usage
## Originally by hackernews user & github e12e
image="${1}"
sz="100px"
for space in sRGB RGB HSV LAB
do
# I think it should be possible to do this without
# writing
# tiff-images to disk in-between -- but having a
# look at the
# resulting images next to the original is
# actually quite nice
# gives some idea of the differences
# colorspace makes:
  convert "${image}" -quantize "${space}" +dither -colors 4 "${image}_${space}.tiff"

  echo "Histogram in ${space} colorspace:"

  convert "${image}_${space}.tiff" -format %c histogram:info:- | tee ${space}.txt
   
  echo
done

colortable()
{
    my_space="${1}"

    echo "${my_space}:"
    echo '<table><tr>'

    sed -nre 's/.*(#[0-9A-F]{6}) .*/\1/p' "${my_space}.txt" | while read color
    do
        echo "<td style='width:${sz};height:${sz};background:${color}'>&nbsp;</td>"
    done

    echo '</tr></table>'
}

htmlhead()
{
cat > index.html <<eof
<html>
<pre>
<img src="./${image}" />
eof
}

middle()
{
for space in sRGB RGB HSV LAB
do
    colortable "${space}" >> index.html
done
}

htmlepiloge()
{
cat >> index.html <<eof
</pre>
</html>
eof
}

htmlhead
middle
htmlepiloge
