# Samplear - Sampling Rate Test
Shows differring audio sampling rates and how it affects sound quality

## Dependencies
ffmpeg, ffprobe, and awk

## Usage
`samplear <audio file>`

Sampling rates are divided into 9 increasing increments from 1, 2, 4 to 100%.
Time is divided into equal 9 equal integer parts.
Change both according to need in lines 15-22


*Note* ffmpeg will not do audio sampling below around 1000Hz.
It may or may not break.
